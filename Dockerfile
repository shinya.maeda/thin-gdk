FROM ubuntu:jammy

ARG GO_VERSION=1.20.7
ARG RUBY_VERSION=3.1.4
ARG RUBYGEM_VERSION=3.3.22
ARG PYTHON_VERSION=3.9
ARG GIT_VERSION=2.42.0
ARG NODE_VERSION=16
ARG IDENTIFIER_PATH_IN_CONTAINER=/thin-gdk-in-container
ARG KIT_ROOT_IN_CONTAINER=/thin-gdk
ARG CHROMEDRIVER_VERSION=114.0.5735.90
ARG BAZEL_VERSION=6.1.0

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install -y parallel wget curl libreadline-dev zlib1g-dev \
        build-essential redis-server libpq-dev ed libicu-dev \
        cmake libmysqlclient-dev libsqlite3-dev libre2-dev lsb-core iproute2 net-tools \
        iputils-ping openssh-server pkg-config

RUN apt-get install -y tzdata libpam-krb5 libkrb5-dev

# Install Docker
RUN apt-get update && \
    apt-get install -y apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
RUN add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
RUN apt-get update && \
    apt-get install -y docker-ce docker-ce-cli containerd.io

# Install Git
RUN apt-get install -y libssl-dev libghc-zlib-dev libcurl4-gnutls-dev libexpat1-dev gettext
RUN apt-get remove -y git
RUN cd /usr/src/
RUN wget https://github.com/git/git/archive/v${GIT_VERSION}.tar.gz -O git.tar.gz
RUN tar -xf git.tar.gz
RUN cd git-* && \
    make prefix=/usr/local all && \
    make prefix=/usr/local install

# Install GoLang
RUN cd tmp/
RUN wget https://dl.google.com/go/go${GO_VERSION}.linux-amd64.tar.gz
RUN tar -xvf go${GO_VERSION}.linux-amd64.tar.gz
RUN mv go /usr/local
RUN ln -s /usr/local/go/bin/* /usr/local/bin

# Downgrade OpenSSL to v1.1.1 (Ubuntu 22.10 has 3.0.x OpenSSL by default but it's not supported in ruby 2.7.7)
RUN wget http://security.ubuntu.com/ubuntu/pool/main/o/openssl/openssl_1.1.1f-1ubuntu2.20_amd64.deb
RUN wget http://security.ubuntu.com/ubuntu/pool/main/o/openssl/libssl-dev_1.1.1f-1ubuntu2.20_amd64.deb
RUN wget http://security.ubuntu.com/ubuntu/pool/main/o/openssl/libssl1.1_1.1.1f-1ubuntu2.20_amd64.deb
RUN dpkg -i libssl1.1_1.1.1f-1ubuntu2.20_amd64.deb
RUN dpkg -i libssl-dev_1.1.1f-1ubuntu2.20_amd64.deb
RUN dpkg -i openssl_1.1.1f-1ubuntu2.20_amd64.deb

# Install system ruby
RUN cd ~ && \
    wget https://cache.ruby-lang.org/pub/ruby/${RUBY_VERSION%.*}/ruby-${RUBY_VERSION}.tar.gz && \
    tar -xvzf ruby-${RUBY_VERSION}.tar.gz && \
    cd ruby-${RUBY_VERSION}

RUN cd ~/ruby-${RUBY_VERSION} && \
    ./configure --enable-shared --disable-install-doc && \
    make && \
    make install

# Update rubygem version
RUN gem update --system ${RUBYGEM_VERSION}

# Install PostgreSQL
RUN sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'
RUN wget -q https://www.postgresql.org/media/keys/ACCC4CF8.asc -O - | apt-key add -
RUN apt-get update && apt-get install -y postgresql-13

# Install Yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && \
    apt-get install -y --no-install-recommends yarn

# Install Node
RUN curl -sL https://deb.nodesource.com/setup_${NODE_VERSION}.x | bash -
RUN apt-get update && \
    apt-get install -y --no-install-recommends nodejs

# Install chromedriver
ENV CHROMEDRIVER_DIR /chromedriver
RUN apt-get install -y wget xvfb unzip
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list
RUN apt-get update -y
RUN apt-get install -y google-chrome-stable
RUN mkdir $CHROMEDRIVER_DIR
RUN wget -q --continue -P $CHROMEDRIVER_DIR "http://chromedriver.storage.googleapis.com/$CHROMEDRIVER_VERSION/chromedriver_linux64.zip"
RUN unzip $CHROMEDRIVER_DIR/chromedriver* -d $CHROMEDRIVER_DIR

# Install Bazel (For KAS)
RUN apt install apt-transport-https curl gnupg
RUN curl -fsSL https://bazel.build/bazel-release.pub.gpg | gpg --dearmor > bazel.gpg
RUN mv bazel.gpg /etc/apt/trusted.gpg.d/
RUN echo "deb [arch=amd64] https://storage.googleapis.com/bazel-apt stable jdk1.8" | tee /etc/apt/sources.list.d/bazel.list
RUN apt update && apt install bazel-${BAZEL_VERSION}
RUN ln -s /usr/bin/bazel-${BAZEL_VERSION} /usr/bin/bazel

# Install Python

RUN python3 --version
RUN apt-get update && \
    apt-get install -y software-properties-common python3-pip
RUN add-apt-repository ppa:deadsnakes/ppa
RUN apt-cache policy python${PYTHON_VERSION}
RUN apt-get install -y python${PYTHON_VERSION} python${PYTHON_VERSION}-distutils python${PYTHON_VERSION}-dev
RUN which python3
RUN ln -sf /usr/bin/python${PYTHON_VERSION} /usr/bin/python
RUN ln -sf /usr/bin/pip3 /usr/bin/pip
RUN python3 --version

EXPOSE 8181

ENV ENABLE_SPRING=1
ENV PATH $CHROMEDRIVER_DIR:$PATH

COPY . ${KIT_ROOT_IN_CONTAINER}
WORKDIR ${KIT_ROOT_IN_CONTAINER}

# Create identifier that this VM is guest
RUN echo "true" > ${IDENTIFIER_PATH_IN_CONTAINER}
